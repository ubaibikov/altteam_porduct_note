{assign var="product_note" value=fn_get_altteam_product_note($order_info.order_id)}
<div class="clearfix">
    <hr>
    <div class="product_note clearfix">
        {foreach $product_note as $_pnote}
            <div class="span6">
                <label for="notes">{__("product_note_to")} : <a href="{"products.update?product_id=`$_pnote.product_id`"|fn_url}">{$_pnote.product_name}</a></label>
                <textarea class="span12" name="update_order[product_note][{$_pnote.note_id}][{$_pnote.product_id}]" id="product_note" cols="40" rows="3">{$_pnote.product_note}</textarea>
            </div>    
        {/foreach}
    </div>
</div>
<br>