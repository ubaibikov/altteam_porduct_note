<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/


if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_altteam_product_note_get_cart_product_data_post(&$hash, &$product, $skip_promotion, &$cart, $auth, $promotion_amount, &$_pdata, $lang_code)
{
    if (!empty($cart['products'][$hash]['product_note'])) {
        $_pdata['product_note'] = $cart['products'][$hash]['product_note'];
    }
}

function fn_altteam_product_note_post_add_to_cart(&$product_data, &$cart, $auth, $update, &$ids)
{
    foreach ($ids as $_id => $product_id) {
        foreach ($product_data as $_pdata) {
            if (!empty($product_data[$product_id]['product_note'])) {
                $cart['products'][$_id]['product_note'] = $product_data[$product_id]['product_note'];
            } else {
                $cart['products'][$_id]['product_note'] = $_pdata['product_note'];
            }
        }
    }
}

function fn_altteam_product_note_create_order_details_post(&$order_id, &$cart, $order_details, $extra, $popularity)
{
    foreach ($cart['products'] as $product) {
        if (!empty($product['product_note'])) {
            db_query('UPDATE ?:order_details SET product_note=?s WHERE order_id = ?i AND product_id', $product['product_note'], $order_id, $product['product_id']);
        }
    }
}

function fn_altteam_product_note_update_order_details_post($params, $order_info, $edp_data, $force_notification)
{
    if(!empty($params['update_order']['product_note']) || !isset($params['update_order']['product_note'])){
        $product_notes = $params['update_order']['product_note'];
        foreach($product_notes as $note_id => $product_note){
            $product_id = key($product_note);
            db_query("UPDATE ?:order_details SET product_note = ?s WHERE product_id = ?i AND order_id = ?i AND item_id = ?i",$product_note[$product_id],$product_id,$order_info['order_id'],$note_id);
        }
    }
}

//[HOOKS] products order_details frontend | backend
function fn_get_altteam_product_note($order_id)
{
    $note_data = db_get_array('SELECT item_id, product_id, product_note FROM ?:order_details WHERE order_id = ?i', $order_id);

    foreach ($note_data as $_note_data) {
        $product_note[] = [
            'note_id' => $_note_data['item_id'],
            'product_id' => $_note_data['product_id'],
            'product_name' => fn_get_product_name($_note_data['product_id']),
            'product_note' => $_note_data['product_note']
        ];
    }

    return $product_note;
}
