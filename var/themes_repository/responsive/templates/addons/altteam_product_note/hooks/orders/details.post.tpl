{assign var="product_note" value=fn_get_altteam_product_note($order_info.order_id)}
{if $product_note}
    {foreach $product_note as $_pnote}
        <div class="ty-orders-notes">
            <h3 class="ty-subheader">{__("product_note_to")} : <a href="{"products.view?product_id=`$_pnote.product_id`"|fn_url}">{$_pnote.product_name}</a></h3>
            <div class="ty-orders-notes__body">
                <span class="ty-caret"><span class="ty-caret-outer"></span><span class="ty-caret-inner"></span></span>
                {$_pnote.product_note|nl2br nofilter}
            </div>
        </div>
    {/foreach}
{/if}