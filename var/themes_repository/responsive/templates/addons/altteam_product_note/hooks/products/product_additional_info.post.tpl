<div class="ty-cart-content__product_note">
    <textarea name="cart_products[{$key}][product_note]" id="product_note_{$key}"  placeholder="{__("product_note")}" cols="15" rows="2" >{$product.product_note}</textarea>
<!--product_note_{$key}--></div>
<script>
    (function (_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context) {
            $("#product_note_{$key}").on('change',function(){
                $('.ty-btn--recalculate-cart').click();
            });
        })
    })(Tygh, Tygh.$);
</script>